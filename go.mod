module modernc.org/lexer

go 1.18

require (
	golang.org/x/exp v0.0.0-20181106170214-d68db9428509
	modernc.org/fileutil v1.2.0
)
